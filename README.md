# DSA Membership API

This is the backend for the membership portal.

It uses Auth0 for authentication / authorization and MailGun for sending email.
However these features are disabled by default for local development.

# Common Installation

We use Docker to run and develop the API. Whether you are running the API locally for
developing the UI, or you are making changes to the API, I suggest you use Docker.

1. **[Install docker-compose](https://docs.docker.com/compose/install/)**
2. **Create your `.env` config file for the project**
    ```
    cp example.env .env
    # Edit .env and replace the NO_AUTH_EMAIL with your email address, as well as the SUPER_USER_* variables.
    # Please change MYSQL_ROOT_PASSWORD as well.
    ```

# Run with Docker

If you don't need to make code changes to the API, the easiest way to get started is to
just use docker compose to run the services. We created a dead-simple command for this:

```
make
```

On first run, this will set up the necessary databases, database tables, and docker images and containers.
On subsequnt runs, it will re-use the pieces that are already set up to get you going quickly.

# Testing

Tests run through docker with `make unittest`. There is a `pytest.ini` file to customize and is
currently set up to output `print` statements during test runs.

# Local database access

* download [sequel pro](https://www.sequelpro.com/)
* ensure the database is running through Docker
* connect to host `127.0.0.1`, username `root`, database `dsa`, and use the `MYSQL_ROOT_PASSWORD` from the `.env` file
* you can connect via the command line with `mysql --user=root --host=127.0.0.1 dsa -p` and use the `MYSQL_ROOT_PASSWORD` from the `.env` file when prompted

# Debug with Docker

If you want to run shell in a docker container, run

```
docker-compose run --entrypoint=/bin/sh {name-in-docker-compose.yml}
```

If you to install the API locally, use the `make dev` command instead of `make`. This will start
the database, run any necessary migrations, and then start the app in debug mode so that it will
pickup live updates when you save any changes.

The API can be health checked locally

```
curl http://localhost:8080/health
# Should see {"health": true}
```

# Troubleshooting

Help! I'm seeing some error. What do I do?

1. **Error** Can't install Python 3.6.1 on Mac OSX, I'm seeing `zlib not available`
**Fix**
Download and install or upgrade XCode, and install the command line tools.
```
xcode-select --install
```

