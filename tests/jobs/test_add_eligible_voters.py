from unittest.mock import Mock

from config import SUPER_USER_EMAIL, SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME
from jobs.add_eligible_voters import AddEligibleVoters
from membership.database.base import engine, metadata, Session
from membership.database.models import Election, Member, Role


class TestAddEligibleVoters:
    job = AddEligibleVoters()
    job.send_invite = Mock()

    def setup(self):
        metadata.create_all(engine)

        session = Session()
        # set up for auth
        member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
        )
        session.add(member)

        election = Election(
            name='Official Chapter Pizza Topping',
            status='draft',
            number_winners=1
        )
        session.add(election)

        role1 = Role(member=member, committee_id=None, role='member')
        role2 = Role(member=member, committee_id=1, role='active')
        session.add_all([role1, role2])

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_eligible_member_ids(self):
        session = Session()
        election = session.query(Election).get(1)
        eligible_member_ids = self.job.eligible_member_ids(session, election)
        assert eligible_member_ids == [1]

    def test_eligible_member_ids_no_election(self):
        session = Session()
        eligible_member_ids = self.job.eligible_member_ids(session, None)
        assert eligible_member_ids == [1]

    def test_add_voters(self):
        session = Session()
        self.job.add_voters(1, [1], session)
