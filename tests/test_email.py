from config import SUPER_USER_EMAIL
import httpretty
import json
from membership.database.base import engine, metadata, Session
from membership.database.models import Email, ForwardingAddress, Member, Role
from membership.web.base_app import app
from tests.http_utils import RequestChecker
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse


class TestEmail:

    @classmethod
    def setup_class(cls):
        metadata.create_all(engine)
        cls.app = Client(app, BaseResponse)
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role = Role()
        role.member = m
        role.role = 'admin'
        session.add(role)
        session.commit()
        session.close()

    def teardown_method(self, method):
        session = Session()
        try:
            session.query(ForwardingAddress).delete()
            session.query(Email).delete()
            session.commit()
        finally:
            session.close()

    @classmethod
    def teardown_class(cls):
        metadata.drop_all(engine)

    def create_email(self, request_checker):
        payload = {'email_address': 'tech@somechapter.org',
                   'forwarding_addresses': [
                       'joe.schmoe@example.com',
                       'jane.doe@example.com'
                   ]}

        mailgun_payload = [
            ('priority', '0'),
            ('description', 'Forwarding rule for tech@somechapter.org'),
            ('expression', 'match_recipient("tech@somechapter.org")'),
            ('action', 'forward("joe.schmoe@example.com")'),
            ('action', 'forward("jane.doe@example.com")')
        ]
        request_checker.add_request(httpretty.POST, 'https://api.mailgun.net/v3/routes',
                                    mailgun_payload, {'route': {'id': 'someid'}})
        self.app.post('/emails', data=json.dumps(payload), content_type='application/json')

    @httpretty.activate
    def test_create_email(self):
        request_checker = RequestChecker()
        self.create_email(request_checker)
        request_checker.check_empty()
        session = Session()
        emails = session.query(Email).all()
        assert len(emails) == 1
        email = emails[0]
        assert email.email_address == 'tech@somechapter.org'
        assert len(email.forwarding_addresses) == 2
        assert email.forwarding_addresses[0].forward_to == 'joe.schmoe@example.com'
        assert email.forwarding_addresses[1].forward_to == 'jane.doe@example.com'
        assert email.external_id == 'someid'
        session.close()

    @httpretty.activate
    def test_list_emails(self):
        request_checker = RequestChecker()
        self.create_email(request_checker)
        session = Session()
        email = session.query(Email).all()[0]
        results = [{'id': email.id, 'email_address': email.email_address,
                    'forwarding_addresses': [f.forward_to for f in email.forwarding_addresses]}]
        res = self.app.get('/emails')
        assert json.loads(res.get_data()) == results

    @httpretty.activate
    def test_update_email(self):
        request_checker = RequestChecker()
        self.create_email(request_checker)
        session = Session()
        email = session.query(Email).all()[0]
        payload = {'forwarding_addresses': [
            'joe.schmoe@example.com',
            'lisa@example.com'
        ]}
        mailgun_payload = [
            ('priority', '0'),
            ('description', 'Forwarding rule for tech@somechapter.org'),
            ('expression', 'match_recipient("tech@somechapter.org")'),
            ('action', 'forward("joe.schmoe@example.com")'),
            ('action', 'forward("lisa@example.com")')
        ]
        request_checker.add_request(httpretty.PUT, 'https://api.mailgun.net/v3/routes/someid',
                                    mailgun_payload, {'route': {'id': 'someid'}})
        self.app.put('/emails/' + str(email.id),
                     data=json.dumps(payload), content_type='application/json')
        request_checker.check_empty()
        session.refresh(email)
        assert email.email_address == 'tech@somechapter.org'
        assert len(email.forwarding_addresses) == 2
        assert email.forwarding_addresses[0].forward_to == 'joe.schmoe@example.com'
        assert email.forwarding_addresses[1].forward_to == 'lisa@example.com'
        assert email.external_id == 'someid'
        session.close()

    @httpretty.activate
    def test_delete_email(self):
        request_checker = RequestChecker()
        self.create_email(request_checker)
        session = Session()
        email = session.query(Email).all()[0]
        request_checker.add_request(httpretty.DELETE, 'https://api.mailgun.net/v3/routes/someid')
        self.app.delete('/emails/' + str(email.id))
        request_checker.check_empty()
        emails = session.query(Email).all()
        assert len(emails) == 0
        forwarding_addresses = session.query(ForwardingAddress).all()
        assert len(forwarding_addresses) == 0, "All forwarding addresses should also be deleted"
        session.close()
