import pytest
from unittest.mock import patch

from datetime import datetime, timezone
from typing import List, Optional

from membership.database.base import engine, metadata, Session
from membership.database.models import Attendee, Member, Meeting, Role, Committee
from membership.repos import MeetingRepo
from membership.services import AttendeeService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.services.errors import ValidationError
from membership.util.time import get_current_time
from overrides import overrides


# TODO: use abstract base class with actual repository for interface
class FakeMeetingRepository(MeetingRepo):

    def __init__(self, meetings: List[Meeting]) -> None:
        self._all = meetings
        self._most_recent = None

    @overrides
    def most_recent(
        self,
        session: Session,
        limit: int = 3,
        since: Optional[datetime] = None
    ) -> List[Meeting]:
        if self._most_recent is None:
            if since is None:
                since = get_current_time()

            def meeting_ended_since(meeting: Meeting) -> bool:
                end_time = meeting.end_time

                if end_time == None:
                    return False

                if end_time.tzinfo is None or end_time.tzinfo.utcoffset(end_time) is None:
                    end_time = end_time.replace(tzinfo=timezone.utc)

                return end_time < since

            def sort_by_start_time(meeting: Meeting) -> datetime:
                return meeting.start_time

            self._most_recent = sorted(
                filter(meeting_ended_since, self._all),
                key=sort_by_start_time
            )
        return self._most_recent[-limit:]


class TestSanFranciscoEligibilityService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()

        meeting1 = Meeting(
            id=1,
            name='General Meeting 1',
            short_id=1,
            start_time=datetime(2017, 1, 1),
            end_time=datetime(2017, 1, 1),
        )
        meeting2 = Meeting(
            id=2,
            name='General Meeting 2',
            short_id=2,
            start_time=datetime(2017, 2, 1),
            end_time=datetime(2017, 2, 1),
        )
        meeting3 = Meeting(
            id=3,
            name='General Meeting 3',
            short_id=3,
            start_time=datetime(2017, 3, 1),
            end_time=datetime(2017, 3, 1),
        )

        session.add_all([meeting1, meeting2, meeting3])
        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_members_as_eligible_to_vote_with_attendance(self):
        session = Session()
        meeting1 = session.query(Meeting).get(1)
        meeting2 = session.query(Meeting).get(2)

        expected_member = Member(
            id=1,
            email_address='test+attendance@example.com',
        )
        role = Role(
            member=expected_member,
            role='member'
        )
        expected_member.roles = [role]

        attendee1 = Attendee(
            meeting_id=meeting1.id,
            member=expected_member
        )
        attendee2 = Attendee(
            meeting_id=meeting2.id,
            member=expected_member
        )
        expected_member.meetings_attended = [
            attendee1,
            attendee2,
        ]

        meeting_repository = FakeMeetingRepository([meeting1, meeting2])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        result = service.members_as_eligible_to_vote(
            {},
            [expected_member],
            datetime(2017, 3, 1).replace(tzinfo=timezone.utc)
        )[0]

        assert result.is_eligible is True
        assert result.message == 'eligible (Jan, Feb)'

    def test_members_as_eligible_to_vote_with_active_role(self):
        expected_member = Member(
            id=1,
            email_address='test+activerole@example.com',
            meetings_attended=[],
            roles=[],
        )
        role1 = Role(
            member=expected_member,
            role='member',
            committee_id=None
        )
        role2 = Role(
            member=expected_member,
            role='active',
            committee_id=1
        )
        expected_member.roles = [role1, role2]
        meeting_repository = FakeMeetingRepository([])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        result = service.members_as_eligible_to_vote({}, [expected_member])[0]

        assert result.is_eligible is True
        assert result.message == 'eligible'

    def test_eligibility_message_for_nonmember(self):
        expected_member = Member(
            id=1,
            email_address='test+nonmember@example.com',
            meetings_attended=[],
        )
        role = Role(
            member=expected_member,
            role='active',
            committee_id=1
        )
        expected_member.roles = [role]

        meeting_repository = FakeMeetingRepository([])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        result = service.members_as_eligible_to_vote({}, [expected_member])[0]

        assert result.is_eligible is False
        assert result.message == 'not eligible'

    def test_eligibility_message_for_inactive_member(self):
        expected_member = Member(
            id=1,
            email_address='test+inactive@example.com',
            meetings_attended=[],
        )
        role = Role(
            member=expected_member,
            role='member'
        )
        expected_member.roles = [role]

        meeting_repository = FakeMeetingRepository([])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        result = service.members_as_eligible_to_vote({}, [expected_member])[0]

        assert result.is_eligible is False
        assert result.message == 'not eligible'

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 3, 1).replace(tzinfo=timezone.utc)
    )
    def test_update_eligibility_to_vote_at_attendance(self, get_current_time_mock):
        session = Session()

        expected_member = Member(
            id=1,
            meetings_attended=[],
        )

        role_member = Role(
            member=expected_member,
            role='member'
        )
        expected_member.roles = [role_member]

        meeting_repository = FakeMeetingRepository([])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        meeting = attendee_service.attend_meeting(expected_member, 3, session)
        attendee = session.query(Attendee).filter_by(member=expected_member, meeting=meeting).one()

        assert attendee.eligible_to_vote is None

        committee = Committee(id=1)
        role_active = Role(
            member=expected_member,
            role='active',
            committee=committee
        )
        expected_member.roles = [role_member, role_active]

        service.update_eligibility_to_vote_at_attendance(session, attendee=attendee)

        assert attendee.eligible_to_vote is True

        session.delete(role_member)
        session.delete(role_active)

        service.update_eligibility_to_vote_at_attendance(session, attendee=attendee)

        assert attendee.eligible_to_vote is False

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(3005, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_update_eligibility_to_vote_past_end_time(self, get_current_time_mock):
        session = Session()

        expected_member = Member(
            id=1,
            meetings_attended=[],
        )

        meeting_repository = FakeMeetingRepository([])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        meeting = attendee_service.attend_meeting(expected_member, 3, session)

        with pytest.raises(
            ValidationError,
            match=r'May not update eligibility to vote after meeting has ended.'
        ):
            service.update_eligibility_to_vote_at_attendance(
                session,
                member=expected_member,
                meeting=meeting
            )

    def test_update_eligibility_to_vote_with_no_end_time(self):
        session = Session()

        meeting_no_end_time = Meeting(
            id=4,
            name='Neverending General Meeting',
            short_id=4,
            start_time=datetime(2017, 3, 1)
        )

        expected_member = Member(
            id=1,
            meetings_attended=[]
        )
        role1 = Role(
            member=expected_member,
            role='member',
            committee_id=None
        )
        role2 = Role(
            member=expected_member,
            role='active',
            committee_id=1
        )
        expected_member.roles = [role1, role2]

        session.add(meeting_no_end_time)

        meeting_repository = FakeMeetingRepository([meeting_no_end_time])
        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        meeting = attendee_service.attend_meeting(expected_member, 4, session)
        attendee = session.query(Attendee).filter_by(member=expected_member, meeting=meeting).one()

        service.update_eligibility_to_vote_at_attendance(session, attendee=attendee)

        assert attendee.eligible_to_vote is True
