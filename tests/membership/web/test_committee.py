import json

from config import SUPER_USER_EMAIL
from datetime import datetime
from membership.database.base import engine, metadata, Session
from membership.database.models import Committee, Email, Member, NationalMembershipData, Role
from membership.web.base_app import app
from tests.flask_utils import get_json, post_json
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse


class TestCommittees:

    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role1 = Role(member=m, role='member', date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role='admin', date_created=datetime(2016, 1, 1))
        email = Email(external_id=1, email_address=SUPER_USER_EMAIL, committee_id=1)
        session.add_all([role1, role2, email])
        membership = NationalMembershipData(
                member=m,
                ak_id='73750',
                first_name='Huey',
                last_name='Newton',
                address_line_1='123 Main St',
                city='Oakland',
                country='United States',
                zipcode='94612',
                dues_paid_until=datetime(2020, 6, 1, 0),
              )
        session.add(membership)

        committee = Committee(id=1, name='Testing Committee')

        session.add(committee)

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_get_committees(self):
        response = get_json(self.app, '/committee/list')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [{
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True
        }]

    def test_add_member_role(self):
        payload = {
            'member_id': 1,
            'committee_id': 1,
            'role': 'admin',
        }
        response = post_json(self.app, '/member/role', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success'}

    def test_add_committee(self):
        payload = {
            'name': 'Committee on Mock Data',
            'admin_list': [SUPER_USER_EMAIL]
        }
        response = post_json(self.app, '/committee', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['created'] == {
            'id': 2,
            'name': 'Committee on Mock Data',
            'email': None,
            'viewable': True,
            'admins': [{'id': 1, 'name': ''}],
            'members': [],
            'active': []
        }

        session = Session()
        committee = session.query(Committee).filter_by(name=payload['name']).one_or_none()
        assert committee is not None
        assert session.query(Role).filter_by(committee=committee, member_id=1, role='admin')\
                      .count() == 1

    def test_get_committee(self):
        response = get_json(self.app, '/committee/1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'admins': [],
            'members': [],
            'active': []
        }

    def test_get_committee_not_found(self):
        response = get_json(self.app, '/committee/2')
        assert response.status_code == 404

    def test_request_committee_membership_where_committee_has_email(self):
        response = post_json(self.app, '/committee/1/member_request')
        assert response.status_code == 200

    def test_request_committee_membership_no_email_with_committee_bad_response(self):
        response = post_json(self.app, '/committee/2/member_request')
        assert response.status_code == 400
