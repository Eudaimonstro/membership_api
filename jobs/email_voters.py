#!/usr/bin/env python3
import argparse
from datetime import datetime
from overrides import overrides
import sys
from typing import IO, List

from config import EMAIL_DOMAIN, PORTAL_URL
from jobs import Job
from membership.database.base import Session
from membership.database.models import Election, Member, Role
from membership.util.email import send_member_emails
from membership.util.time import CHAPTER_TIME_ZONE


class EmailVoters(Job):
    """
    Email members who are eligible for an upcoming election.
    """

    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument('ids', nargs='*', type=int)
        parser.add_argument('-e', type=int)
        parser.add_argument('-t', type=argparse.FileType('r'))
        parser.add_argument('-o', '--output', type=argparse.FileType('w'), default=sys.stdout)

    @overrides
    def run(self, config: dict) -> None:
        outfile: IO[str] = config.get('output', sys.stdout)
        session = Session()

        template: str = config.get('t').read()

        election_id: int = config.get('e')
        election: Election = session.query(Election).get(election_id)

        eligible_member_ids: List[int] = config.get('ids')
        if len(eligible_member_ids) == 0:
            eligible_member_ids = map(lambda v: v.member_id, election.voters)
        eligible_member_set: Set[int] = set(eligible_member_ids)

        members: List[Member] = session.query(Member) \
            .join(Member.roles) \
            .filter(
                    Member.email_address != None,
                    Role.role == 'member',
                    Role.committee_id == None,
                   ) \
            .all()

        eligible_members = [m for m in members if m.id in eligible_member_set]

        self.send_eligible_reminder_emails(eligible_members, election, template, out=outfile)

    def format_date(self, utc_dt: datetime) -> str:
        """
        Format a date for the email.
        :param utc_dt: the datetime
        :return: a human-readable string
        """
        dt = utc_dt.astimezone(CHAPTER_TIME_ZONE)
        return f"{dt:%A}, {dt:%B} {dt.day} at {dt.hour}:{dt:%M}{dt:%p} {dt:%Z}".strip()

    def confirm_dialog(self) -> bool:
        return input("Continue (Y/n)? ").lower() in {'y', ''}

    def send_eligible_reminder_emails(
            self,
            eligible_members: List[Member],
            election: Election,
            template: str,
            out: IO[str]) -> None:
        """
        Send reminder emails to a list of members.
        :param eligible_members: a pre-screened list of members to email
        :param election: the upcoming election
        :param template: template for the email body
        :param out: output stream
        """

        sender = f'DSA SF Online Voting <tech@{EMAIL_DOMAIN}>'
        recipient_variables: Dict[Member, Dict[str, str]] = {
            member: {'first_name': member.first_name}
            for member in eligible_members
        }
        template_variables: Dict[str, str] = {
            'election_name': election.name,
            'election_start_date': self.format_date(election.voting_begins),
            'election_end_date': self.format_date(election.voting_ends),
            'election_link': f"{PORTAL_URL}/vote/{election.id}",
            'portal_url': PORTAL_URL,
            'quorum': int(len(eligible_members) / 2) + 1,
        }
        body = template.format(**template_variables)
        subject = f"%recipient.first_name%, you are eligible to vote on {election.name}!"

        print(f"Subject: {subject}", file=out)
        print(f"Body:\n{body}", file=out)
        print(f"Recipients: {len(eligible_members)}", file=out)

        if self.confirm_dialog():
            send_member_emails(sender, subject, body, recipient_variables)
