from typing import Optional

from config.lib import from_env, throw

USE_EMAIL: bool = from_env.get_bool('USE_EMAIL', False)
EMAIL_DOMAIN: str = from_env.get_str('EMAIL_DOMAIN', 'dsasf.org')
EMAIL_API_KEY: Optional[str] = from_env.get_str('EMAIL_API_KEY', throw if USE_EMAIL else None)
MAILGUN_URL: str = from_env.get_str('MAILGUN_URL', 'https://api.mailgun.net/v3/routes')

SUPPORT_EMAIL_ADDRESS: str = from_env.get_str(
    'SUPPORT_EMAIL_ADDRESS',
    f'tech-support@{EMAIL_DOMAIN}',
)
