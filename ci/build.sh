#!/bin/sh
set -efx

##
# Build variables
##
if [ -z "$REGISTRY" ]; then
    echo "Missing environment variable REGISTRY from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$TAG" ]; then
    echo "Missing environment variable TAG from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$NAMESPACE" ]; then
    NAMESPACE="dsasanfrancisco"
fi

# Copy the .env file
if [ -f ".env" ]; then
    # Must be testing locally
    echo "Found existing .env file. Are you running this locally?"
    echo "Please backup your .env file first and then run this script"
    exit 1
fi
cp ci/ci.env .env

# Build the images
docker-compose build

docker tag "$NAMESPACE/membership_api:latest" "$REGISTRY/$NAMESPACE/membership_api:$TAG"
docker tag "$NAMESPACE/membership_migrate:latest" "$REGISTRY/$NAMESPACE/membership_api/migrate:$TAG"
docker tag "$NAMESPACE/membership_test:latest" "$REGISTRY/$NAMESPACE/membership_api/test:$TAG"
docker push "$REGISTRY/$NAMESPACE/membership_api:$TAG"
docker push "$REGISTRY/$NAMESPACE/membership_api/migrate:$TAG"
docker push "$REGISTRY/$NAMESPACE/membership_api/test:$TAG"
