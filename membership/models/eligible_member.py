from membership.util.delegator import SimpleDelegator


class MemberAsEligibleToVote(SimpleDelegator):

    def __init__(self, *args, is_eligible, message):
        super().__init__(*args)
        self.is_eligible = is_eligible
        self.message = message
