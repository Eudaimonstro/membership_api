from typing import List, Optional
import dateutil.parser

from membership.database.base import Session
from membership.database.models import Election
from membership.models import ElectionStateMachine


class ElectionService:

    def list_elections(self, session: Session) -> List[Election]:
        return list(session.query(Election).all())

    def find_election_by_id(self, session: Session, election_id: int) -> Optional[Election]:
        return session.query(Election).get(election_id)

    def set_times(self, election: Election, json: dict) -> Election:
        if 'start_time' in json:
            if json['start_time'] is None:
                election.voting_begins_epoch_millis = None
            else:
                start_time = dateutil.parser.parse(json['start_time'])
                election.voting_begins_epoch_millis = round(start_time.timestamp() * 1000)

        if 'end_time' in json:
            if json['end_time'] is None:
                election.voting_ends_epoch_millis = None
            else:
                end_time = dateutil.parser.parse(json['end_time'])
                election.voting_ends_epoch_millis = round(end_time.timestamp() * 1000)

        return election

    def next_election_transitions(self, election: Election) -> List[str]:
        return ElectionStateMachine(election.status).next_transitions()

    def transition_election_as(self, session: Session, transition: str, election_id: int) \
            -> (bool, Optional[Election]):
        election: Election = self.find_election_by_id(session, election_id)
        if election is None:
            return False, None

        state_machine = ElectionStateMachine(election.status)
        if transition not in state_machine.next_transitions():
            return False, election

        getattr(state_machine, transition)()
        election.status = state_machine.status
        session.merge(election)
        session.commit()

        return True, election
