from overrides import overrides

from membership.database.models import EmailTemplate
from membership.schemas import format_datetime, JsonObj
from membership.schemas.rest.model import RestModel


class EmailTemplateRest(RestModel[EmailTemplate]):
    model = EmailTemplate
    schema = {
        'type': 'object',
        'properties': {
            'id': {
                'type': ['integer', 'string'],
            },
            'name': {
                'type': 'string',
            },
            'topic_id': {
                'type': ['integer', 'string'],
            },
            'subject': {
                'type': 'string',
            },
            'body': {
                'type': 'string',
            },
        },
        'required': ['name']
    }

    @classmethod
    @overrides
    def format(cls, template: EmailTemplate) -> JsonObj:
        return {
            'id': template.id,
            'name': template.name,
            'topic_id': template.topic_id,
            'subject': template.subject,
            'body': template.body,
            'last_updated': format_datetime(template.last_updated)
        }
