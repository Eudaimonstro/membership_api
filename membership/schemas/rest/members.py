from collections import defaultdict

from typing import Callable, Iterable, List, Optional

from membership.database.base import Session
from membership.database.models import Meeting, Member, Role, ProxyToken, ProxyTokenState
from membership.models import Authorization, MemberAsEligibleToVote, MemberQueryResult
from membership.repos import MeetingRepo
from membership.schemas import JsonObj
from membership.schemas.rest.generic import format_iterable
from membership.services import AttendeeService
from membership.services.eligibility import SanFranciscoEligibilityService


def format_eligible_member_list(
    az: Authorization,
    meeting: Optional[Meeting]=None,
    query_session=None
) -> Callable[[Iterable[MemberAsEligibleToVote]], List[JsonObj]]:
    tokens_by_receiver = defaultdict(list)
    tokens_by_sender = defaultdict(list)
    if meeting and query_session:
        attendee_member_ids = [attendee.member_id for attendee in meeting.attendees]
        accepted_proxy_tokens = (
            query_session.query(ProxyToken)
            .filter(ProxyToken.meeting_id == meeting.id)
            .filter(ProxyToken.state == ProxyTokenState.ACCEPTED)
            .filter(ProxyToken.receiving_member_id.in_(attendee_member_ids))
            .all()
        )
        for token in accepted_proxy_tokens:
            tokens_by_receiver[token.receiving_member_id].append(token)
        for token in accepted_proxy_tokens:
            tokens_by_sender[token.member_id].append(token)

    def format_eligible_member(eligible_member: MemberAsEligibleToVote) -> JsonObj:
        email_address_viewable = az.has_role('admin', member_id=eligible_member.id)
        num_votes = 1 if eligible_member.is_eligible else 0
        num_votes += len(tokens_by_receiver[eligible_member.id])
        num_votes -= len(tokens_by_sender[eligible_member.id])
        return {
            'id': eligible_member.id,
            'name': eligible_member.name,
            'eligibility': {
                'is_eligible': eligible_member.is_eligible,
                'message': eligible_member.message,
                'num_votes': num_votes,
            },
            **({'email': eligible_member.email_address} if email_address_viewable else {})
        }

    return format_iterable(format_eligible_member)


def format_role(role: Role) -> JsonObj:
    return {
        'role': role.role,
        'committee_id': role.committee.id if role.committee else -1,
        'committee_name': role.committee.name if role.committee else 'general',
        # TODO: To be deprecated once the front end no longer depends on 'committee'
        'committee': role.committee.name if role.committee else 'general',
        'date_created': role.date_created
    }


def format_member_query_result(member_query_result: MemberQueryResult, az: Authorization) \
        -> JsonObj:
    return {
        'members': format_eligible_member_list(az)(member_query_result.members),
        'cursor': member_query_result.cursor,
        'has_more': member_query_result.has_more,
    }


def format_member_info(member: Member, az: Authorization) -> JsonObj:
    return {
        'first_name': member.first_name,
        'last_name': member.last_name,
        'biography': member.biography,
        **(
            {'email_address': member.email_address}
            if az.has_role('admin', member_id=member.id)
            else {}
        )
    }


def format_member_basics(member: Member, az: Authorization) -> JsonObj:
    return {
        'id': member.id,
        'info': format_member_info(member, az),
        'roles': [format_role(role) for role in member.roles],
    }


def format_member_full(session: Session, member: Member, az: Authorization) -> JsonObj:
    if az.member_id == member.id:
        membership = max(member.memberships_usa, key=lambda m: m.dues_paid_until, default=None)
    else:
        membership = None

    eligibility_service = SanFranciscoEligibilityService(
        meetings=MeetingRepo(Meeting),
        attendee_service=AttendeeService()
    )
    is_eligible = eligibility_service.is_member_eligible(session, member)

    return {
        **format_member_basics(member, az),
        'do_not_call': member.do_not_call,
        'do_not_email': member.do_not_email,
        'is_eligible': is_eligible,
        'meetings': [
            {
                'meeting_id': attendee.meeting.id,
                'name': attendee.meeting.name,
            } for attendee in member.meetings_attended
        ],
        'votes': [
            {
                'election_id': eligible_vote.election_id,
                'election_name': eligible_vote.election.name,
                'election_status': eligible_vote.election.status,
                'voted': eligible_vote.voted,
            } for eligible_vote in member.eligible_votes
        ],
        **(
            {'membership':
                {
                    'address': membership.address,
                    'phone_numbers': [phone_number.number for phone_number in member.phone_numbers],
                    'dues_paid_until': membership.dues_paid_until
                }
             } if membership is not None else {}
        )
    }
