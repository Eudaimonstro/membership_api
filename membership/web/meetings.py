import datetime
import logging

from flask import Blueprint, jsonify, request
from typing import List, Optional  # NOQA

from membership.database.models import Attendee, Meeting, Member, ProxyToken, ProxyTokenState
from membership.models import AuthContext
from membership.repos import MeetingRepo
from membership.schemas.rest.members import format_eligible_member_list
from membership.services import ValidationError, MeetingService, AttendeeService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.util.email import send_proxy_nomination_action
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound

meeting_api = Blueprint('meeting_api', __name__)
meeting_service = MeetingService()
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(
    meetings=meeting_repository,
    attendee_service=attendee_service
)


def meeting_to_dict(meeting: Meeting):
    return {
        'id': meeting.id,
        'name': meeting.name,
        'committee_id': meeting.committee_id,
        'code': meeting.short_id,
        'landing_url': meeting.landing_url,
        'start_time': meeting.start_time,
        'end_time': meeting.end_time,
    }


@meeting_api.route('/meeting', methods=['POST'])
@requires_auth()
def create_meeting(ctx: AuthContext):
    try:
        name = request.json['name']
        committee_id = request.json.get('committee_id')
        ctx.az.verify_admin(committee_id)

        meeting = meeting_service.add_meeting(name, committee_id, ctx.session)
        meeting = meeting_service.set_meeting_fields(meeting, request.json, ctx.session)
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)

    return jsonify({
        'status': 'success',
        'meeting': meeting_to_dict(meeting),
    })


@meeting_api.route('/meeting/list', methods=['GET'])
@requires_auth()
def get_meetings(ctx: AuthContext):
    meetings: List[Meeting] = ctx.session.query(Meeting).order_by(Meeting.id.desc()).all()
    result = [meeting_to_dict(m) for m in meetings]
    return jsonify(result)


@meeting_api.route('/meeting', methods=['PATCH'])
@requires_auth()
def update_meeting(ctx: AuthContext):
    # TODO: Do better generic model validation
    meeting_id = str(request.json['meeting_id'])
    if not meeting_id:
        return BadRequest('Missing "meeting_id"')

    meeting = meeting_service.find_meeting_by_id(meeting_id, ctx.session)
    committee_id = None if meeting is None else meeting.committee_id
    ctx.az.verify_admin(committee_id)
    if not meeting:
        return NotFound('Could not find meeting with id={}'.format(meeting_id))

    try:
        meeting_service.set_meeting_fields(meeting, request.json, ctx.session)
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)

    return jsonify({
        'status': 'success',
        'meeting': meeting_to_dict(meeting),
    })


@meeting_api.route('/meeting/attend', methods=['POST'])
@requires_auth()
def attend_meeting(ctx: AuthContext):
    try:
        short_id: int = int(request.json['meeting_short_id'])
    except ValueError as e:
        return BadRequest("Cannot parse 'meeting_short_id' as int: {}".format(str(e)))

    member_id = request.json.get('member_id', ctx.requester.id)

    if ctx.requester.id != member_id:
        ctx.az.verify_admin()

    try:
        meeting: Optional[Meeting] = attendee_service.attend_meeting_with_short_id(
            member_id,
            short_id,
            ctx.session,
        )

        if meeting is None:
            return NotFound('Meeting with short_id={} does not exist'.format(short_id))
        elif meeting.is_general_meeting:
            eligibility_service.update_eligibility_to_vote_at_attendance(
                ctx.session,
                member=member_id,
                meeting=meeting
            )
    except ValidationError as e:
        # Attendance may be recorded retroactively; although the method which
        # sets eligibility during attandance will raise an error if the meeting
        # has already ended, we will quietly ignore it and leave the
        # eligibility status untouched.
        if e.key != SanFranciscoEligibilityService.ErrorCodes.MEETING_ENDED:
            return BadRequest(e.message)

    return jsonify({'status': 'success', 'landing_url': meeting.landing_url})


@meeting_api.route('/meetings/<int:meeting_id>', methods=['GET'])
@requires_auth()
def get_meeting(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))
    return jsonify(meeting_to_dict(meeting))


@meeting_api.route('/meetings/<int:meeting_id>/attendees', methods=['GET'])
@requires_auth()
def get_meeting_attendees(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id
    ctx.az.verify_admin(committee_id)
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    if meeting.is_general_meeting:
        result = format_eligible_member_list(
            az=ctx.az,
            meeting=meeting,
            query_session=ctx.session,
        )(
            SanFranciscoEligibilityService.attendees_as_eligible_to_vote(meeting)
        )
    else:
        result = [
            {
                'id': attendee.member_id,
                'name': attendee.member.name,
            } for attendee in meeting.attendees
        ]

    return jsonify(result)


@meeting_api.route('/meetings/<int:meeting_id>/attendees/<int:member_id>', methods=['PATCH'])
@requires_auth()
def update_meeting_attendance(ctx: AuthContext, meeting_id: int, member_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id
    ctx.az.verify_admin(committee_id)
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    attendees = ctx.session.query(Attendee).filter_by(
        meeting_id=meeting_id,
        member_id=member_id,
    ).all()

    try:
        for attendee in attendees:
            if request.json.get('update_eligibility_to_vote', False):
                eligibility_service.update_eligibility_to_vote_at_attendance(
                    ctx.session,
                    attendee=attendee
                )
    except ValidationError as e:
        return BadRequest(e.message)

    return jsonify({'status': 'success'})


@meeting_api.route('/meetings/<int:meeting_id>/attendees/<int:member_id>', methods=['DELETE'])
@requires_auth()
def remove_meeting_attendee(ctx: AuthContext, meeting_id: int, member_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id
    ctx.az.verify_admin(committee_id)
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    attendees = ctx.session.query(Attendee).filter_by(
        meeting_id=meeting_id,
        member_id=member_id,
    ).all()
    for attendee in attendees:
        ctx.session.delete(attendee)
    ctx.session.commit()

    return jsonify({'status': 'success'})


@meeting_api.route('/meetings/<int:meeting_id>/attendee', methods=['POST'])
@requires_auth()
def add_attendee_from_kiosk(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))
    email_address = request.json['email_address']
    if not email_address:
        return BadRequest('You must supply an email address to check in')
    member = ctx.session.query(Member).filter_by(email_address=email_address).one_or_none()
    if not member:
        member = Member()
        member.first_name = request.json['first_name']
        member.last_name = request.json['last_name']
        member.email_address = email_address
        ctx.session.add(member)
        ctx.session.commit()

    attendee_service.attend_meeting(member.id, meeting, ctx.session, return_none_on_error=True)

    if meeting.is_general_meeting:
        # Attendance may be recorded retroactively; although the method
        # which sets eligibility during attandance will raise an error if
        # the meeting has already ended, we will quietly ignore it and
        # leave the eligibility status untouched.
        try:
            eligibility_service.update_eligibility_to_vote_at_attendance(
                ctx.session,
                member=member,
                meeting=meeting
            )
        except ValidationError as e:
            if e.key != SanFranciscoEligibilityService.ErrorCodes.MEETING_ENDED:
                raise e

    return jsonify({'status': 'success'})


@meeting_api.route('/meetings/<int:meeting_id>/proxy-token', methods=['POST'])
@requires_auth()
def generate_meeting_proxy_token(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    if not eligibility_service.is_member_eligible(ctx.session, ctx.requester):
        return BadRequest(
            'You are not eligible to vote, so you cannot nominate a proxy to vote for you',
        )

    existing_tokens = (
        ctx.session.query(ProxyToken)
        .filter(ProxyToken.member_id == ctx.requester.id)
        .filter(ProxyToken.meeting_id == meeting_id)
        .all()
    )
    if existing_tokens:
        return BadRequest('You have already nominated a proxy for {}'.format(meeting_id))

    proxy_token = ProxyToken(
        member_id=ctx.requester.id,
        meeting_id=meeting_id,
    )
    ctx.session.add(proxy_token)
    ctx.session.commit()

    return jsonify({
        'proxy_token_id': proxy_token.id,
        'state': proxy_token.state.value,
    })


@meeting_api.route(
    '/meetings/<int:meeting_id>/proxy-token/<string:proxy_token_id>',
    methods=['GET'],
)
@requires_auth()
def check_proxy_token_state(ctx: AuthContext, meeting_id: int, proxy_token_id: str):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    proxy_token = ctx.session.query(ProxyToken).filter_by(id=proxy_token_id).one_or_none()
    if not proxy_token:
        return NotFound('Proxy token {} does not exist'.format(proxy_token_id))

    member = ctx.session.query(Member).filter_by(id=proxy_token.member_id).one_or_none()

    return jsonify({
        'proxy_token_id': proxy_token.id,
        'nominator_id': member.id,
        'nominator_name': member.name,
        'recently_acted_member_id': proxy_token.receiving_member_id,
        'state': proxy_token.state.value,
    })


@meeting_api.route(
    '/meetings/<int:meeting_id>/proxy-token/<string:proxy_token_id>/<string:verb>',
    methods=['POST'],
)
@requires_auth()
def act_on_meeting_proxy_token(ctx: AuthContext, meeting_id: int, proxy_token_id: str, verb: str):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))
    if meeting.end_time < datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc):
        return BadRequest('Meeting {} has already ended'.format(meeting_id))

    proxy_token = ctx.session.query(ProxyToken).filter_by(id=proxy_token_id).one_or_none()
    if not proxy_token:
        return NotFound('Proxy token {} does not exist'.format(proxy_token_id))
    if proxy_token.member_id == ctx.requester.id:
        return BadRequest('You cannot act on your own proxy token')
    if proxy_token.state == ProxyTokenState.ACCEPTED:
        return BadRequest('Proxy token {} has already been accepted'.format(proxy_token_id))

    # TODO: if ctx.requester.id has already ACCEPTED at least N other proxy nominations
    # for this meeting, they cannot ACCEPT any more

    nominating_member = ctx.session.query(Member).filter_by(id=proxy_token.member_id).one()

    if verb == 'accept':
        proxy_token.state = ProxyTokenState.ACCEPTED
    elif verb == 'reject':
        proxy_token.state = ProxyTokenState.REJECTED
    else:
        # TODO: add a REVOKE flow, where the nominator can (no matter the .state)
        # act as if they never nominated anyone
        return BadRequest('Action {} is not supported, you may only accept or reject'.format(verb))
    proxy_token.receiving_member_id = ctx.requester.id
    ctx.session.add(proxy_token)
    ctx.session.commit()

    try:
        send_proxy_nomination_action(meeting, nominating_member, ctx.requester, proxy_token)
        email_sent = True
    except Exception as e:
        email_sent = False
        logging.error(
            f"Could not send proxy token update email to member.id={nominating_member.id}: {e}",
        )

    return jsonify({
        'proxy_token_id': proxy_token.id,
        'nominator_id': nominating_member.id,
        'nominator_name': nominating_member.name,
        'state': proxy_token.state.value,
        'email_sent': email_sent,
    })
